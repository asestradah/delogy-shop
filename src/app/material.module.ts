
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        MatMenuModule,
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatSnackBarModule,
        MatTableModule,
        MatDialogModule,
        MatCheckboxModule,
      } from '@angular/material';

const myMaterialModule = [
        MatInputModule,
        MatFormFieldModule,
        MatCardModule,
        MatButtonModule,
        MatMenuModule,
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatSnackBarModule,
        MatTableModule,
        MatDialogModule,
        MatCheckboxModule,
];

@NgModule({
  declarations: [],
  imports: [CommonModule, myMaterialModule],
  exports: [myMaterialModule]
})
export class MaterialModule { }

import { Component, OnInit } from '@angular/core';
import {trigger, style, transition, animate, state, keyframes} from '@angular/animations';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  animations: [
    // aparecer tarjeta
     trigger('enterState', [
      state('void', style({
        opacity: 0
      })),
      transition(':enter', [
        animate('0.5s', style({
        opacity: 1
        }))
      ])
     ]),
    ]
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

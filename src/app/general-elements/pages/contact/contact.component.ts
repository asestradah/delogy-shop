import { Component, OnInit } from '@angular/core';
import {trigger, style, transition, animate, state, keyframes} from '@angular/animations';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  animations: [
    // aparecer tarjeta
     trigger('enterState', [
      state('void', style({
        opacity: 0
      })),
      transition(':enter', [
        animate('0.5s', style({
        opacity: 1
        }))
      ])
     ]),
    ]
})
export class ContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

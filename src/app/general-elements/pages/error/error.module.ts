import { ButtonBackModule } from './../../elements/button-back/button-back.module';
import { MaterialModule } from './../../../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErrorRoutingModule } from './error-routing.module';
import { ErrorComponent } from './error.component';


@NgModule({
  declarations: [ErrorComponent],
  imports: [
    CommonModule,
    ErrorRoutingModule,
    MaterialModule,
    ButtonBackModule,
  ]
})
export class ErrorModule { }

import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';
import {trigger, style, transition, animate, state, keyframes} from '@angular/animations';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.css'],
  animations: [
    // aparecer tarjeta
     trigger('enterState', [
      state('void', style({
        opacity: 0
      })),
      transition(':enter', [
        animate('0.5s', style({
        opacity: 1
        }))
      ])
     ]),
     // mover icono
     trigger('cursorState', [
      // state('activeL', style({
      //   transform: 'translateX(-10%)'
      // })),
      // state('neutral', style({
      //   transform: 'translateX(0)'
      // })),
      // state('activeR', style({
      //   transform: 'translateX(+10%)'
      // })),
      // transition('activeL=>neutral', animate('1s')),
      // transition('neutral=>inactive', animate('1s')),
      // transition('active<=>inactive', animate('1s')),
      transition('*=>*', [animate('0.5s', keyframes([
        style({transform: 'translateY(-5%)'}),
        style({transform: 'translateY(+5%)'}),
        style({transform: 'translateY(-5%)'}),
        style({transform: 'translateY(+5%)'}),
        style({transform: 'translateY(-5%)'}),
        style({transform: 'translateY(0%)'}),
      ]))]),
     ]),
    ]
  })

export class IconComponent implements OnInit {
  public state = 'inactive';
@Input() nameModule: string;
@Input() iconModule: string;

  constructor(private location: Router) { }

  ngOnInit(): void {
  }

  cursorOnButton() {
    this.state = this.state === 'active' ? 'inactive' : 'active';
  }

  prueba(prueba: string) {
    switch (prueba) {
      case 'Articulo':
        this.location.navigateByUrl('/about');
        break;
        case 'Empresa':
          this.location.navigateByUrl('/contact');
          break;

      default:
        this.location.navigateByUrl('/menu');
        break;
    }
    console.log(prueba);
  }
}

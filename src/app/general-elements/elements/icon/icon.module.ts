import { MaterialModule } from './../../../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconRoutingModule } from './icon-routing.module';
import { IconComponent } from './icon.component';


@NgModule({
  declarations: [IconComponent],
  imports: [
    CommonModule,
    IconRoutingModule,
    MaterialModule,
  ],
  exports: [
    IconComponent,
  ],
})
export class IconModule { }

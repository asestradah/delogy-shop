import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ButtonBackComponent } from './button-back.component';

const routes: Routes = [{ path: '', component: ButtonBackComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ButtonBackRoutingModule { }

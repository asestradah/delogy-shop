import { MaterialModule } from './../../../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonBackRoutingModule } from './button-back-routing.module';
import { ButtonBackComponent } from './button-back.component';


@NgModule({
  declarations: [ButtonBackComponent],
  imports: [
    CommonModule,
    ButtonBackRoutingModule,
    MaterialModule
  ],
  exports: [
    ButtonBackComponent
  ]
})
export class ButtonBackModule { }

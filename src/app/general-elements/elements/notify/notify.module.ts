import { NgbootstrapModule } from './../../../ngbootstrap.module';
import { MaterialModule } from './../../../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotifyRoutingModule } from './notify-routing.module';
import { NotifyComponent } from './notify.component';


@NgModule({
  declarations: [NotifyComponent],
  imports: [
    CommonModule,
    NotifyRoutingModule,
    MaterialModule,
    NgbootstrapModule,
  ],
  exports: [NotifyComponent]
})
export class NotifyModule { }

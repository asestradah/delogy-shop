import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.css']
})
export class NotifyComponent implements OnInit {

  constructor(private toastr: ToastrService) {}
  // constructor(private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  showNotify(answer: number, message: string) {
    switch (answer) {
      case 200:
        this.toastr.success('OK', ' ' + message);
        break;
      case 300:
        this.toastr.info('', ' ' + message);
        break;
      case 400:
        this.toastr.error(' ' + message);
        break;
      default:
        this.toastr.warning('Error inesperado', 'Se desonocen los detalles del error, contacte a soporte técnico');
        break;
    }

  }

  // openSnackBar(message: string, action: string) {
  //   this.snackBar.open(message, action, {
  //     duration: 2000,
  //   });
  // }

}

import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import {trigger, style, transition, animate, state, keyframes} from '@angular/animations';

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.css'],
  animations: [
    // aparecer tarjeta
     trigger('enterState', [
      state('void', style({
        opacity: 0
      })),
      transition(':enter', [
        animate('0.5s', style({
        opacity: 1
        }))
      ])
     ]),
    ],
  providers: [NgbCarouselConfig]
})
export class SlideshowComponent implements OnInit {

  images = [1, 2, 3].map((n) => `../../../../assets/slider/img${n}.jpg`);
  constructor(config: NgbCarouselConfig) {
    config.showNavigationArrows = false;
    config.showNavigationIndicators = false;
  }

  ngOnInit(): void {
  }

}

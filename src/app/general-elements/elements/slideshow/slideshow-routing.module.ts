import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SlideshowComponent } from './slideshow.component';

const routes: Routes = [{ path: '', component: SlideshowComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SlideshowRoutingModule { }

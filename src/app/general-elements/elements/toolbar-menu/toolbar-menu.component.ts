import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar-menu',
  templateUrl: './toolbar-menu.component.html',
  styleUrls: ['./toolbar-menu.component.css']
})
export class ToolbarMenuComponent implements OnInit {
  public opened = false;
  constructor() { }

  ngOnInit(): void {
  }

  closeNav() {
    this.opened = this.opened === false ? true : false;

  }

}

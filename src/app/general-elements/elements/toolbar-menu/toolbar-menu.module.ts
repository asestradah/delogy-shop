import { MaterialModule } from './../../../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolbarMenuRoutingModule } from './toolbar-menu-routing.module';
import { ToolbarMenuComponent } from './toolbar-menu.component';


@NgModule({
  declarations: [ToolbarMenuComponent],
  imports: [
    CommonModule,
    ToolbarMenuRoutingModule,
    MaterialModule,
  ],
  exports: [
    ToolbarMenuComponent,
  ]
})
export class ToolbarMenuModule { }

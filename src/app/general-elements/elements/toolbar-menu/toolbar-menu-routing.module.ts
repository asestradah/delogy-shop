import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ToolbarMenuComponent } from './toolbar-menu.component';

const routes: Routes = [{ path: '', component: ToolbarMenuComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToolbarMenuRoutingModule { }

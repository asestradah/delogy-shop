import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomecontainerComponent } from './homecontainer.component';

const routes: Routes = [{ path: '', component: HomecontainerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomecontainerRoutingModule { }

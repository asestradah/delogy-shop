import { SlideshowModule } from './../../general-elements/elements/slideshow/slideshow.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomecontainerRoutingModule } from './homecontainer-routing.module';
import { HomecontainerComponent } from './homecontainer.component';
import { LogInModule } from 'src/app/auth/log-in/log-in.module';


@NgModule({
  declarations: [HomecontainerComponent],
  imports: [
    CommonModule,
    HomecontainerRoutingModule,
    SlideshowModule,
    LogInModule,
  ]
})
export class HomecontainerModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

const myBootstrapModule = [
  NgbCarouselModule,
  NgbAlertModule
];

@NgModule({
  declarations: [],
  imports: [CommonModule,myBootstrapModule],
  exports: [myBootstrapModule]
})
export class NgbootstrapModule { }

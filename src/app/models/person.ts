export class HttpSign {
  person: Array<any>;
  user: string;
  password: string;
}

export class Person {
  name: string;
  last_name: string;
}

import { HomepageComponent } from './homepage/homepage.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  // Pagina principal donde esta el login y el registro
  {path: '', component: HomepageComponent,
  children: [
    { path: 'home',
    loadChildren: () => import('./homepage/homecontainer/homecontainer.module').then(m => m.HomecontainerModule) },
    { path: 'about',
    loadChildren: () => import('./general-elements/pages/about/about.module').then(m => m.AboutModule)},
    { path: 'contact',
    loadChildren: () => import('./general-elements/pages/contact/contact.module').then(m => m.ContactModule) },
    {path: '', redirectTo: 'home', pathMatch: 'full'}
  ]},

  // Menu de usuario con componentes
  {path: 'mainpage', component: MainpageComponent,
  children: [
    { path: 'menu', loadChildren: () => import('./mainpage/menu/menu.module').then(m => m.MenuModule) },
    {path: '', redirectTo: 'menu', pathMatch: 'full'}
  ]},

  // Componentes extra
  { path: 'about',
  loadChildren: () => import('./general-elements/pages/about/about.module').then(m => m.AboutModule)},
  { path: 'contact',
  loadChildren: () => import('./general-elements/pages/contact/contact.module').then(m => m.ContactModule) },
  { path: 'error',
  loadChildren: () => import('./general-elements/pages/error/error.module').then(m => m.ErrorModule) },
  { path: 'icon',
  loadChildren: () => import('./general-elements/elements/icon/icon.module').then(m => m.IconModule) },
  { path: 'button-back',
  loadChildren: () => import('./general-elements/elements/button-back/button-back.module').then(m => m.ButtonBackModule) },
  { path: 'notify',
  loadChildren: () => import('./general-elements/elements/notify/notify.module').then(m => m.NotifyModule) },
  { path: 'toolbar',
  loadChildren: () => import('./general-elements/elements/toolbar/toolbar.module').then(m => m.ToolbarModule) },
  { path: 'toolbar-menu',
  loadChildren: () => import('./general-elements/elements/toolbar-menu/toolbar-menu.module').then(m => m.ToolbarMenuModule) },
  { path: 'home', loadChildren: () => import('./homepage/homepage.module').then(m => m.HomepageModule) },
  { path: 'mainpage', loadChildren: () => import('./mainpage/mainpage.module').then(m => m.MainpageModule) },

  { path: 'slideshow', loadChildren: () => import('./general-elements/elements/slideshow/slideshow.module').then(m => m.SlideshowModule) },

  { path: 'homecontent', loadChildren: () => import('./homepage/homecontainer/homecontainer.module').then(m => m.HomecontainerModule) },

  { path: 'log-in', loadChildren: () => import('./auth/log-in/log-in.module').then(m => m.LogInModule) },

  { path: 'sig-in', loadChildren: () => import('./auth/sig-in/sig-in.module').then(m => m.SigInModule) },

  { path: '**', loadChildren: () => import('./general-elements/pages/not-found/not-found.module').then(m => m.NotFoundModule) },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

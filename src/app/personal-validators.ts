import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, ValidationErrors} from '@angular/forms';

export const passwordEqual: ValidatorFn = (
  control: FormGroup
): ValidationErrors | null => {
  const password = control.get('password');
  const confirmarPassword = control.get('password2');
  return password.value === confirmarPassword.value ? null : { notEquals: true };
};

import { NotifyModule } from './../../general-elements/elements/notify/notify.module';
import { CreateUserService } from './create-user.service';
import { MaterialModule } from './../../material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SigInRoutingModule } from './sig-in-routing.module';
import { SigInComponent } from './sig-in.component';


@NgModule({
  declarations: [SigInComponent],
  imports: [
    CommonModule,
    SigInRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    NotifyModule,
    ],
    providers: [CreateUserService]
})
export class SigInModule { }

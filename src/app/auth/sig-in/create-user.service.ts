import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CreateUserService {
  public URL = 'http://www.api.delogy.mx/';
  constructor(private http: HttpClient) { }

  postDevice(so: string): Observable<any> {
    return this.http.post(this.URL + 'devices', {
    os: so
    });
  }

  sigIn(namePerson: string, lastNamePerson: string, emailPerson: string, pass: string, tokenDevice: string): Observable <any> {
    // this.httpSign.user = emailPerson;
    // this.httpSign.password = pass;
    // const per = [namePerson, lastNamePerson];
    // this.httpSign.person = per;
    // console.log(this.httpSign);
    return this.http.post(this.URL + 'users', {
      person: {
        name: namePerson,
        last_name: lastNamePerson,
      },
      user: emailPerson,
      password: pass,
    },
    {headers: { Authorization: 'Bearer ' + tokenDevice, 'Content-Type': 'application/json'}
    },
    );
  }
}

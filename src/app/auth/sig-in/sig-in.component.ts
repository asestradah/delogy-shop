import { passwordEqual } from './../../personal-validators';

import { NotifyComponent } from './../../general-elements/elements/notify/notify.component';
import { CreateUserService } from './create-user.service';
import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, ValidatorFn, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-sig-in',
  templateUrl: './sig-in.component.html',
  styleUrls: ['./sig-in.component.css']
})
export class SigInComponent implements OnInit {
  siginForm: FormGroup;
  pattern = '/^[a-zA-Z0-9_\-\.~]{2,}@[a-zA-Z0-9_\-\.~]{2,}\.[a-zA-Z]{2,4}$/g';

  constructor(
    private createUser: CreateUserService,
    private location: Router,
    private n: NotifyComponent,
    private fb: FormBuilder,
    ) {
  }

  ngOnInit(): void {
    this.siginForm = this.fb.group({
      name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(this.pattern)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      password2: ['', [Validators.required, Validators.minLength(5)]]},
      { validators: passwordEqual}
      );
  }

  checkPassword(): boolean {
    return this.siginForm.hasError('notEquals') &&
    this.siginForm.get('password').dirty &&
    this.siginForm.get('password2').dirty;
  }

  onSigin(form: any) {
    if (form.password !== form.password2) {
      this.n.showNotify(400, 'Verifique que las contraseñas coincidan');

    } else {
      this.createUser.postDevice('web').subscribe(
        res => {
          localStorage.setItem('token_device', res.data.token);
        },
        error => {
          console.log(error);
        }
        );
      this.createUser.sigIn(form.name, form.last_name, form.email, form.password, localStorage.getItem('token_device')).subscribe(
          resp => {
            console.log(resp);
            this.location.navigateByUrl('/home');
          },
          error => {
            console.log(error);
          }
        );
    }
  }

}

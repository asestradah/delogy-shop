import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public URL = 'http://www.api.delogy.mx/';
  constructor(private http: HttpClient) { }

  logIn(username: string, pass: string, clientkey: string): Observable <any>{
    return this.http.post(this.URL + 'auth', {
      user: username,
      password: pass,
      client_key: clientkey,
    });
  }
}


import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { NotifyComponent } from '../../general-elements/elements/notify/notify.component';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  public key = '.bf4%a9fSDFe7%FFeS22wa.<9daHJH7078(#ce-536bf08f4d>';
  constructor(private login: LoginService, private location: Router, private n: NotifyComponent) {
  }
loginForm = new FormGroup({
  email: new FormControl('', Validators.required),
  password: new FormControl('', Validators.required)
});

  ngOnInit(): void {
  }

  onLogin(form: any) {
    this.login.logIn(form.email, form.password, this.key).subscribe(
      res => {
        localStorage.setItem('token_user', res.data.token);
        this.n.showNotify(res.status, res.customMessage);
        this.location.navigateByUrl('/mainpage');
      },
      error => {
        console.log(error);

      }
    );
  }
}

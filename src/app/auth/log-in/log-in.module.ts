import { NotifyModule } from './../../general-elements/elements/notify/notify.module';
import { LoginService } from './login.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogInRoutingModule } from './log-in-routing.module';
import { LogInComponent } from './log-in.component';


@NgModule({
  declarations: [LogInComponent],
  imports: [
    CommonModule,
    LogInRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    NotifyModule,
  ],
  exports: [
    LogInComponent,
  ],
  providers: [LoginService]
})
export class LogInModule { }

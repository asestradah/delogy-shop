import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  modules: any;
  limit: number;

  constructor() {
    this.modules = [
      {name : 'Articulo', icon : 'shopping_basket'},
      {name : 'Empresa', icon: 'business'},
      {name : 'Articulo', icon : 'shopping_basket'},
      {name : 'Empresa', icon: 'business'},
      {name : 'Articulo', icon : 'shopping_basket'},
      {name : 'Empresa', icon: 'business'},
      // {name : '', icon: ''},
    ];
    this.limit = this.modules.length;
  }

  ngOnInit(): void {
  }

}

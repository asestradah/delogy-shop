import { MaterialModule } from './../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainpageRoutingModule } from './mainpage-routing.module';
import { MainpageComponent } from './mainpage.component';
import { ToolbarMenuModule } from '../general-elements/elements/toolbar-menu/toolbar-menu.module';


@NgModule({
  declarations: [MainpageComponent],
  imports: [
    CommonModule,
    MainpageRoutingModule,
    ToolbarMenuModule,
  ]
})
export class MainpageModule { }

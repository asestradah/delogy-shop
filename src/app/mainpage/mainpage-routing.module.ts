import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainpageComponent } from './mainpage.component';

const routes: Routes = [{ path: '', component: MainpageComponent,
children: [
  { path: 'menu',
  loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule) },

  { path: 'about',
  loadChildren: () => import('../general-elements/pages/about/about.module').then(m => m.AboutModule)},

  { path: 'contact',
  loadChildren: () => import('../general-elements/pages/contact/contact.module').then(m => m.ContactModule) },
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainpageRoutingModule { }
